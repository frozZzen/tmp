#ifndef LateBindFunctor_hpp
#define LateBindFunctor_hpp

#include <stdexcept>
#include <utility>
#include <tuple>

/**
 * This functor class wraps either a class member function or a non-member function
 * and passes over the bound arguments to the function when calling the call operator.
 * The arguments, the function pointer end the owner object, the last one only in case
 * of class member functions, can be bound at any time before calling the call operator.
 */
template <class Owner, class ReturnType, class... Args>
class LateBindFunctor;

template <class ReturnType, class... Args>
class LateBindFunctor<void, ReturnType(Args...)>
{
public:
  using FunctionPtr = ReturnType(*)(Args...);

  LateBindFunctor()                       : mFunction(nullptr),   mIsArgsSet(false)   { }
  LateBindFunctor(FunctionPtr function)   : mFunction(function),  mIsArgsSet(false)   { }

  void setFunction(FunctionPtr function)  { mFunction = function; }
  void setArguments(Args... args)         { mArguments = std::tuple<Args...>(args...); mIsArgsSet = true; }
  
  bool isFunctionSet() const              { return mFunction != nullptr; }
  bool isArgumentsSet() const             { return mIsArgsSet; }

  ReturnType operator()()
  {
    if (mFunction == nullptr || !mIsArgsSet)
    {
      throw std::runtime_error("LateBindFunctor cannot be called before setting the function pointer and the arguments!");
    }
    return callFunctionWithArgumentIndexes(std::index_sequence_for<Args...>());
  }

private:
  template <std::size_t... Args2>
  ReturnType callFunctionWithArgumentIndexes(const std::index_sequence<Args2...>&)
  {
    return (*mFunction)(std::get<Args2>(mArguments)...);
  }

  FunctionPtr         mFunction;
  std::tuple<Args...> mArguments;
  bool                mIsArgsSet;
};

template <class Owner, class ReturnType, class... Args>
class LateBindFunctor<Owner, ReturnType(Args...)>
{
public:
  using FunctionPtr = ReturnType(Owner::*)(Args...);

  LateBindFunctor()                                     : mOwner(nullptr),  mFunction(nullptr),   mIsArgsSet(false)   { }
  LateBindFunctor(Owner* owner)                         : mOwner(owner),    mFunction(nullptr),   mIsArgsSet(false)   { }
  LateBindFunctor(Owner* owner, FunctionPtr function)   : mOwner(owner),    mFunction(nullptr),   mIsArgsSet(false)   { }

  void setOwner(Owner* owner)                                   { mOwner = owner; }
  void setFunction(FunctionPtr function)                        { mFunction = function; }
  void setOwnerAndFunction(Owner* owner, FunctionPtr function)  { mOwner = owner; mFunction = function; }
  void setArguments(Args... args)                               { mArguments = std::tuple<Args...>(args...); mIsArgsSet = true; }
  
  bool isOwnerSet() const                                       { return mOwner != nullptr; }
  bool isFunctionSet() const                                    { return mFunction != nullptr; }
  bool isArgumentsSet() const                                   { return mIsArgsSet; }

  ReturnType operator()()
  {
    if (mOwner == nullptr || mFunction == nullptr || !mIsArgsSet)
    {
      throw std::runtime_error("LateBindFunctor cannot be called before setting the owner, the function pointer and the arguments!");
    }
    return callFunctionWithArgumentIndexes(std::index_sequence_for<Args...>());
  }

private:
  template <std::size_t... Args2>
  ReturnType callFunctionWithArgumentIndexes(const std::index_sequence<Args2...>&)
  {
    return (mOwner->*mFunction)(std::get<Args2>(mArguments)...);
  }

  Owner*              mOwner;
  FunctionPtr         mFunction;
  std::tuple<Args...> mArguments;
  bool                mIsArgsSet;
};

#endif //LateBindFunctor_hpp
