#include "LateBindFunctor.hpp"

#include <iostream>

struct ExampleOwner
{
  ExampleOwner(int mul)
    : mul(mul)
  {
  }
  
  void function(std::string s, int i)
  {
    std::cout << s << i * mul << std::endl;
  }
    
private:
  int mul;
};

int nonMemberFunction(double d, int mul)
{
  return d * mul;
}

int main()
{
  ExampleOwner o{3};
  LateBindFunctor<ExampleOwner, void(std::string, int)> f1;
  
  f1.setOwner(&o);
  f1.setFunction(&ExampleOwner::function);
  f1.setArguments("20", 21);
  f1();
  
  LateBindFunctor<void, int(double, int)> f2;
  
  f2.setFunction(&nonMemberFunction);
  f2.setArguments(0.001, 100'0);
  std::cout << f2() << std::endl;
  
  return 0;
}

/* PROGRAM OUTPUT:
2063
1
*/
